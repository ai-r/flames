import numpy as np
from data_pb2 import BLANK, TREE, ON_FIRE, UNKNOWN, CHARRED, DOUSED

def Tree_count(cell_matrix):
    start_tree_count = np.count_nonzero(cell_matrix == TREE)
    large_matrix = np.full((74,130),1,np.uint8)
    large_matrix[1:73,1:129] = cell_matrix
    roam_list = [[0,-1],[1,0],[0,1],[-1,0],[-1,-1],[1,-1],[1,1],[-1,1]]
    found_trees = True
    while found_trees:
        found_trees = False
        for i in range(1,73):
            for j in range(1,129):
                if large_matrix[i][j] == ON_FIRE:
                    for point in roam_list:
                        if large_matrix[i+point[0]][j+point[1]] == TREE:
                            found_trees = True
                            large_matrix[i+point[0]][j+point[1]] = ON_FIRE
    super_tree_count = np.count_nonzero(large_matrix == TREE)
    return start_tree_count,super_tree_count