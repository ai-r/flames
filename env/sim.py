import numpy as np
from env.level import LevelState
from flames_common import create_mover, advance_mover, set_mover_from_proto
from data_pb2 import DOUSED, ON_FIRE
import random
import time
import sys
from math import floor

TICK_DURATION = 1.0/2.0
MOVE_TICK_DURATION = 1.0/60.0
DRONE_RADIUS = 5
HELICOPTER_RADIUS = 10

DRONE_SPEED = 2.0
HELI_SPEED = 2.0

class Drone:
    def __init__(self, x, y):
        self.mover = create_mover()
        self.mover.pos.x = x
        self.mover.pos.y = y
        self.mover.dest.x = x
        self.mover.dest.y = y
        self.override = None
        self.override_start_time = 0

    def set_path_override(self, path, t):
        self.override = path
        self.override_start_time = t

class Helicopter:
    def __init__(self, x, y):
        self.mover = create_mover()
        self.mover.pos.x = x
        self.mover.pos.y = y
        self.mover.dest.x = x
        self.mover.dest.y = y


# A context-free simulation of the flames environment
class FlamesSim:
    def __init__(self, drone_count):
        # The residual amount of simulation time left between updates
        self.remaining_time = 0
        self.level = LevelState()

        self.drones = [Drone(
            x=random.uniform(10, 60),
            y=random.uniform(120, 130)) for x in range(drone_count)]

        self.heli = Helicopter(x=36, y=110)
        self.reveal()

    def set_drone_dst(self, drone_id, x, y):
        self.drones[drone_id].mover.dest.x = x
        self.drones[drone_id].mover.dest.y = y

    def set_helicopter_dst(self, x, y):
        self.heli.mover.dest.x = x
        self.heli.mover.dest.y = y

    # Monotonically advance time
    def tick(self):
        for i in range(0, 30):
            # Advance the movers
            for d in self.drones:
                advance_mover(d.mover, MOVE_TICK_DURATION, DRONE_SPEED)

            advance_mover(self.heli.mover, MOVE_TICK_DURATION, HELI_SPEED)

        # Propagate fire
        self.level.update_temperature()


        
        # deal with doused cells
        x = self.heli.mover.pos.x
        y = self.heli.mover.pos.y
        radius = int(HELICOPTER_RADIUS/2)
        radius_sq = radius*radius
        for i in range(-radius, radius):
            for j in range(-radius, radius):
                dst_sq = i*i + j*j
                if dst_sq < radius_sq:
                    cell_x = floor(x+i)
                    cell_y = floor(y+j)
                    # following set directly, better to bring this in from somewhere, maybe shape of truth matrix for example
                    w = 72
                    h = 128
                    if (cell_x >= 0 and cell_x < w and
                       cell_y >= 0 and cell_y < h):
                        if self.level.truth_matrix[cell_x][cell_y] == ON_FIRE:
                            self.level.truth_matrix[cell_x][cell_y] = DOUSED

    def reveal(self):
        reveal_locs = [
          (x.mover.pos.x, x.mover.pos.y, DRONE_RADIUS) for x in self.drones
        ]

        reveal_locs.append(
          (self.heli.mover.pos.x, self.heli.mover.pos.y, HELICOPTER_RADIUS)
        )
        return self.level.reveal(reveal_locs)


    # Advances time
    def update(self, dt):
        self.remaining_time += dt
        while self.remaining_time > TICK_DURATION:
            self.remaining_time -= TICK_DURATION
            self.tick()
        return self.reveal()
