import cog_settings
from data_pb2 import Ai_DroneAction, BLANK, TREE, ON_FIRE, UNKNOWN, CHARRED, DOUSED

from cogment import Agent, GrpcServer
import random
import math
from math import floor
import numpy as np
import cv2
from agents.smartest.utils import get_direction, get_fire_matrices, my_random
import pickle
import sys
#from agents.smartest.viewer import Viewer


DECISION_RANGE = 10.0
DRONE_RADIUS = 5

#cellstate2display = [UNKNOWN,BLANK,ON_FIRE,TREE,CHARRED,DOUSED]
cellstate2display = [(128,128,128),(255,255,255),(0,0,255),(0,255,0),(45,82,160),(32,165,218)]

class Ai_Drone(Agent):
    VERcSIONS = {"ai_drone": "1.0.0"}
    actor_class = cog_settings.actor_classes.ai_drone

    def __init__(self, trial, actor):
        super().__init__(trial, actor)
        self.x = 36
        self.y = 64
        self.images = []
        self.myactions = []
        self.count = 1



    def decide(self, observation):
        if self.count < 1000:
            self.count += 1

        action = Ai_DroneAction()


        my_info = observation.drones[self.id_in_class]

        if True:  #self.id_in_class == 0:
            #print('count:',self.count)

            """
            if self.count == 900:
                print('Got to write images')
                with open('images.pkl','wb') as f:
                    pickle.dump(self.images, f)
                    #f.flush()
                    f.close()
                with open('actions.pkl','wb') as f:
                    pickle.dump(self.myactions, f)
                    f.close()
            """
            my_matrix,calc_matrix,fire_count = get_fire_matrices(observation.cells,my_info.position.x,my_info.position.y)
            self.images.append(calc_matrix)
            #fire_count = np.count_nonzero(np.array(calc_matrix.flatten('F')) == 1)


            # if self.id_in_class == 0:
            #      self.viewer.show_(my_matrix,'image')

            if fire_count != 0:   # if there's fire
                action.message = "Monitoring"
                direction = get_direction(calc_matrix.transpose())
                #print("FIRE DIRECTION",fire_direction[direction])
                self.myactions.append(direction)
                self.x = my_info.position.x + direction[0]
                self.y = my_info.position.y + direction[1]
                self.x = max(0, min(self.x, 71))  # fix this
                self.y = max(0, min(self.y, 127))  # fix this
            else:   # if there's no fire
                action.message = "Searching"
                if abs(my_info.position.x - my_info.destination.x) < 1 and abs(my_info.position.y - my_info.destination.y) < 1:
                    #print('NO FIRE NEW DIRECTION')
                    self.myactions.append('NO FIRE NEW DIR')
                    self.x,self.y = my_random()
                    #self.x = random.randint(0,71) 
                    #self.y = random.randint(0,127)  
                else:
                    #print('NO FIRE CONTINUE')
                    self.myactions.append('NO FIRE CONT')
                    self.x = my_info.destination.x
                    self.y = my_info.destination.y

        else:
            # for other drones
            self.x = my_info.position.x
            self.y = my_info.position.y


        # do action
        p = action.path.add()
        p.x = self.x
        p.y = self.y

        return action

    def reward(self, reward):
        pass

    def end(self):
        pass

    

if __name__ == '__main__':
    server = GrpcServer(Ai_Drone, cog_settings)
    server.serve()
