from threading import Thread, Lock
import cv2

import queue
import numpy as np
from data_pb2 import BLANK, TREE, ON_FIRE, UNKNOWN, CHARRED, CHARRED_OLD, BLANK_OLD, TREE_OLD, ON_FIRE_OLD, UNKNOWN_OLD, DOUSED, DOUSED_OLD





# All in BGR format
RED = (0, 0, 255)
GREEN = (0, 255, 0)
BLUE = (255, 0, 0)
WHITE = (255, 255, 255)
BLACK = (0, 0, 0)
GOLDENROD = (32,165,218)
SOFTGOLDENROD = (16, 82, 109)

color_table = np.array([BLACK] * 255, dtype=np.uint8)
color_table[BLANK] = WHITE
color_table[TREE] = GREEN
color_table[ON_FIRE] = RED
color_table[UNKNOWN] = BLACK
color_table[CHARRED] = (58, 121, 199)
color_table[DOUSED] = GOLDENROD
color_table[BLANK_OLD] = (128, 128, 128)
color_table[TREE_OLD] = (0, 128, 0)
color_table[ON_FIRE_OLD] = (0, 0, 128)
color_table[UNKNOWN_OLD] = BLACK
color_table[DOUSED_OLD] = SOFTGOLDENROD

class Viewer:
    op_queue = queue.Queue()
    display_thread = None
    mutex = Lock()
    running = True

    def perform_on_display_thread(function, *args, **kwargs):
        Viewer.op_queue.put((function, args, kwargs))

    def do_kill_display_thread_():
        Viewer.running = False

    def kill_display_thread():
        Viewer.perform_on_display_thread(Viewer.do_kill_display_thread_)

    def update_display_main():
        print("launching viewer thread...")
        while True:
            try:
                function, args, kwargs = Viewer.op_queue.get(timeout=1)
                function(*args, **kwargs)
                Viewer.op_queue.task_done()
                cv2.waitKey(1)
                if not Viewer.running:
                    break
            except queue.Empty:
                cv2.waitKey(1)

    def launch_thread_if_needed():
        Viewer.mutex.acquire()
        if Viewer.display_thread is not None:
            Viewer.mutex.release()
            return
        Viewer.display_thread = Thread(target=Viewer.update_display_main)
        Viewer.display_thread.start()
        Viewer.mutex.release()

    # Creates a new viewer window, bound to a given sim
    def __init__(self):
        Viewer.launch_thread_if_needed()
        #self.win_name = name
        #self.sim = sim
        name = 'test'
        Viewer.perform_on_display_thread(cv2.namedWindow,
                                         name, cv2.WINDOW_NORMAL)

    # Display the view_matrix of the bound sim
    def refresh_from_sim(self):
        image = color_table[self.sim.level.truth_matrix]
        self.show_(image)

    # Tears down the viewer's window
    def destroy(self):
        Viewer.perform_on_display_thread(cv2.destroyWindow, self.win_name)

    # Displays an arbitrary image in the viewer
    def show_(self, img,name):
        rotated = cv2.flip(cv2.rotate(img, rotateCode=0), 1)
        Viewer.perform_on_display_thread(cv2.imshow, name, rotated)


