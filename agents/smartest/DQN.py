import random
import numpy as np
from collections import deque
#import threading

from keras.models import Sequential, load_model
from keras.layers import Dense, Conv2D, MaxPooling2D, Flatten
from keras.optimizers import Adam
import keras

import tensorflow as tf

import os



#global graph #, model
#graph = tf.get_default_graph()
#model = Sequential()
#print("AAA: ", threading.get_ident())

GAMMA = 0.95  
LEARNING_RATE = 0.001  

MEMORY_SIZE = 1000000  
BATCH_SIZE = 20

EXPLORATION_MAX = 1.0 
EXPLORATION_MIN = 0.01
EXPLORATION_DECAY = 0.999  #0.995  

NUM_DIRECTIONS = 8

#INPUT_SHAPE = (9,9,1)

class DQNSolver:

    def __init__(self,obs_space,action_space):
        self.gamma = GAMMA
        self.learning_rate = LEARNING_RATE
        self.exploration_min = EXPLORATION_MIN
        self.exploration_rate = EXPLORATION_MAX
        self.exploration_decay = EXPLORATION_DECAY

        self.memory = deque(maxlen=MEMORY_SIZE)
        tf.keras.backend.clear_session()

        self.graph = tf.get_default_graph()


        with self.graph.as_default():

            self.model = Sequential()
            self.model.add(Conv2D(16, kernel_size=(3, 3),
                             activation='relu',
                             input_shape=obs_space))
            self.model.add(Conv2D(32, (3, 3), activation='relu'))
            self.model.add(MaxPooling2D(pool_size=(2, 2)))
            #model.add(Dropout(0.25))
            self.model.add(Flatten())
            self.model.add(Dense(64, activation='relu'))
            #model.add(Dropout(0.5))
            #self.model.add(Dense(NUM_DIRECTIONS, activation='softmax'))

            self.model.add(Dense(action_space, activation='linear'))

            #self.model.compile(loss=keras.losses.categorical_crossentropy,
            #              optimizer=keras.optimizers.Adadelta(),
            #              metrics=['accuracy'])

            #self.model.compile(loss=keras.losses.categorical_crossentropy,
            #              optimizer=keras.optimizers.Adadelta(lr=self.learning_rate),
            #              metrics=['accuracy'])

            self.model.compile(loss="mse", optimizer=Adam(lr=self.learning_rate),
                metrics=['accuracy'])


            '''
            self.model = Sequential()
            self.model.add(Dense(24, input_shape=(observation_space,), activation="relu"))
            self.model.add(Dense(24, activation="relu"))
            self.model.add(Dense(self.action_space, activation="linear"))
            self.model.compile(loss="mse", optimizer=Adam(lr=self.learning_rate))
            '''
        #model.summary()



    def remember(self, state, action, reward, next_state, done, qvals):
        self.memory.append((state, action, reward, next_state, done, qvals))

    def act(self, state):
        if np.random.rand() < self.exploration_rate:
            randval =  np.array([random.uniform(-1,1) for x in range(8)])
            #return random.randrange(NUM_DIRECTIONS),np.array([0.,0.,0.,0.,0.,0.,0.,0.])
            #print("QQQQQQQQQQQQQQQQQQ",np.argmax(randval), randval)
            return np.argmax(randval), randval
        q_values = self.my_predict(state)
        return np.argmax(q_values[0]), q_values[0]

    def experience_replay(self):
        if len(self.memory) < BATCH_SIZE:
            return
        batch = random.sample(self.memory, BATCH_SIZE)
        for state, action, reward, state_next, terminal,qvals in batch:
            q_update = reward
            if not terminal:
                predict = self.my_predict(state_next)
                q_update = (reward + self.gamma * np.amax(predict[0]))
            q_values = self.my_predict(state)
            #print('QQQQQQQQQQQQQQ',q_values)
            q_values[0][action] = q_update
            with self.graph.as_default():
                self.model.fit(state, q_values, verbose=0)
        self.exploration_rate *= self.exploration_decay
        self.exploration_rate = max(self.exploration_min, self.exploration_rate)

    def my_predict(self, my_state):
        with self.graph.as_default():
            predict = self.model.predict(my_state)
        return predict

    def get_score(self):
        # batch = random.sample(self.memory, BATCH_SIZE)
        batch = self.memory
        y_actions = []
        x_states = []
        for state, action, reward, state_next, terminal, qvals in batch:
            new_state = np.reshape(state, (9,9,1))
            x_states.append(new_state)
            #new_action = np.reshape(action,)
            y_actions.append(qvals)
        x_states = np.array(x_states)
        y_actions = np.array(y_actions)
        #print('XXXXXX:',x_states)
        #print('YYYYYY:',y_actions)
        with self.graph.as_default():
            score = self.model.evaluate(x_states,y_actions,verbose = 0)
        return score

    def save(self,file_name):
        print('Saving Model')
        with self.graph.as_default():
            #self.model.save_weights('./agents/random/model_weights.h5')
            self.model.save_weights(file_name)
        print('Model Saved')

    def load(self,file_name):
        #if os.path.isfile('./agents/random/model_weights.h5'):
        if os.path.isfile(file_name):
            print("Loading Model")
            with self.graph.as_default():
                #self.model.load_weights('./agents/random/model_weights.h5')
                self.model.load_weights(file_name)
            print('Model Loaded')
