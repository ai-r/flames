import cog_settings
from data_pb2 import Ai_DroneAction
from agents.smartest.contour import Ai_Drone as ContourDrone

from cogment import Agent, GrpcServer
import random
import math


DECISION_RANGE = 10.0


class Ai_Drone(Agent):
    VERSIONS = {"ai_drone": "1.0.1"}
    actor_class = cog_settings.actor_classes.ai_drone

    def __init__(self, trial, actor):
        print("spinning up agent")
        super().__init__(trial, actor)
        self.countour_drone = ContourDrone(trial, actor)
        self.get_new_dest = True

    def decide(self, observation):
        return self.countour_drone.decide(observation)

    def reward(self, reward):
        pass

    def end(self):
        pass


if __name__ == '__main__':
    server = GrpcServer(Ai_Drone, cog_settings)
    server.serve()
