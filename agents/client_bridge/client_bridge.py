import cog_settings
from data_pb2 import Loop_HumanAction

from cogment import Agent, GrpcServer
from cogment.api.orchestrator_pb2 import DESCRIPTOR,TrialFeedbackReply,TrialEndReply,TrialStartReply,TrialActionReply,TrialHeartbeatReply
from cogment.api.orchestrator_pb2_grpc import TrialServicer,add_TrialServicer_to_server


import numpy as np
import os
import queue
from concurrent import futures
import grpc
from grpc_reflection.v1alpha import reflection

import threading

import time

init_queue = queue.Queue()

class AgentClientBridge(Agent):
  VERSIONS = {"agent_client_bridge": "1.0.1"}
  actor_class = cog_settings.actor_classes.loop_human

  def __init__(self, trial, actor):
    print("spinning up Agent")
    super().__init__(trial, actor)

    self.obs_ready = threading.Event()
    self.action_ready = threading.Event()
    self.first_time = True
    self.count = 0
    self.key = trial.id + "_subclient_" + str(self.id_in_class)

  def decide(self, observation):
    #print('Agent Count',self.count)
    self.count += 1
    if observation.done:
      print("Observation Done!!")

    # connection section
    if self.first_time:
      self.first_time = False
      init_queue.put(self)
      print('First Time:',self.key)

    # observation section
    self.observation = observation
    self.obs_ready.set()

    self.action_ready.wait()
    self.action_ready.clear()


    return self.action

  def reward(self, reward):
    pass

  def end(self):
    print('AGENT END!!!')
    #self.trial_end_flag = True

class FakeOrchestrator():

  def __init__(self):
    self.agents = {}

  def Version(self):
    pass

  def Start(self, msg, ctx):

    agt = init_queue.get()

    agt.trial_id = agt.key
    self.agents[agt.key] = agt

    rep = TrialStartReply()

    rep.trial_id = agt.key

    agt.obs_ready.wait()
    agt.obs_ready.clear()

    rep.observation.data.snapshot = True
    rep.observation.data.content = agt.observation.SerializeToString()


    return rep

  def Action(self, msg, ctx):

    key = msg.trial_id
    #print('KKKey',key)

    agt = self.agents[msg.trial_id]

    agt.action = Loop_HumanAction()
    agt.action.ParseFromString(msg.action.content)


    agt.action_ready.set()

    agt.obs_ready.wait()
    agt.obs_ready.clear()

    rep = TrialActionReply()

    rep.observation.data.snapshot = True
    rep.observation.data.content = agt.observation.SerializeToString()

    return rep

  def GiveFeedback(self,msg,ctx):
    pass

  def Heartbeat(self,msg,ctx):
    rep = TrialHeartbeatReply()
    return rep

  def End(self,msg,ctx):
    print("Fake Orch Trial End")
    #client_to_agent_q.put("clientend")  # send trial end flag
    rep = TrialEndReply()
    return rep

def dump(obj):
  for attr in dir(obj):
    print("obj.%s = %r" % (attr, getattr(obj, attr)))

if __name__ == '__main__':
  cog_server = GrpcServer(AgentClientBridge, cog_settings)
  cog_server.start()

  server = grpc.server(futures.ThreadPoolExecutor(max_workers=10))
  add_TrialServicer_to_server(FakeOrchestrator(), server)

  # Enable grpc reflection if requested
  SERVICE_NAMES = (
      DESCRIPTOR.services_by_name['Trial'].full_name,
      reflection.SERVICE_NAME,
  )
  reflection.enable_server_reflection(
    SERVICE_NAMES, server)


  server.add_insecure_port('[::]:50051')
  server.start()
  server.wait_for_termination()