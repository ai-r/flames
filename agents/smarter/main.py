import cog_settings

from cogment import Agent, GrpcServer
import random
import math
import time


DECISION_RANGE = 10.0


class Ai_Drone(Agent):
    VERSIONS = {"ai_drone": "1.0.1"}
    actor_class = cog_settings.actor_classes.ai_drone

    def __init__(self, trial, actor):
        print("spinning up agent")
        super().__init__(trial, actor)
        self.ref_time = time.time()
        self.get_new_dest = True
        self.pause = 0

    def decide(self, observation):
        t = time.time()
        dt = t - self.ref_time
        self.ref_time = t
        
        action = Ai_Drone.actor_class.action_space()

        my_info = observation.drones[self.id_in_class]
        if self.get_new_dest:
            action.message = "Choosing"
            if self.pause <= 0:
                self.x = random.randint(0, 71)
                self.y = random.randint(0, 128)
                self.x = max(0, min(self.x, 72))
                self.y = max(0, min(self.y, 128))
                self.get_new_dest = False
            else:
                self.pause -= dt
        else:
            action.message = "Going"
            if abs(my_info.position.x - my_info.destination.x) < 3 and abs(my_info.position.y - my_info.destination.y) < 3:
                self.get_new_dest = True
                self.pause = 2.0

        p = action.path.add()
        p.x = self.x
        p.y = self.y

        return action

    def reward(self, reward):
        pass

    def end(self):
        pass


if __name__ == '__main__':
    server = GrpcServer(Ai_Drone, cog_settings)
    server.serve()
