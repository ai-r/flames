import cog_settings
from data_pb2 import Human_PlaneAction

from cogment import Agent, GrpcServer

import numpy as np
from agents.fake_human.utils2 import get_gradient_matrix, find_maxima,get_next_move,scale_matrix
from agents.fake_human.viewer import Viewer
from agents.fake_human.utils import get_direction, get_fire_matrices, my_random
import os

from math import floor

ENABLE_VIEWER = bool(os.getenv('ENABLE_VIEWER', False))

class FakeHuman(Agent):
    VERSIONS = {"fake_human": "1.0.1"}
    actor_class = cog_settings.actor_classes.human_plane

    def __init__(self, trial, actor):
        print("spinning up agent")
        super().__init__(trial, actor)
        #self.viewer = Viewer()
        self.color_gradient_matrix = np.full((72,128,3),255,np.uint8)
        self.mono_gradient_matrix = np.zeros((72,128),np.uint16)
        self.count = 0
        self.x = 36 
        self.y = 64
        self.reset_state = True
        self.search_state = 'random'
        self.viewer = None
        if ENABLE_VIEWER:
            self.viewer = Viewer()




    def decide(self, observation):
        self.count += 1
        action = Human_PlaneAction()

        self.color_gradient_matrix,self.mono_gradient_matrix = get_gradient_matrix(observation.cells,self.color_gradient_matrix,self.mono_gradient_matrix,self.count)

        if self.viewer is not None:
            self.viewer.show_(scale_matrix(self.color_gradient_matrix,scale_factor=8),'image')

        my_matrix,calc_matrix,fire_count = get_fire_matrices(observation.cells,observation.copter.position.x,observation.copter.position.y)
        #self.viewer.show_(my_matrix,'image')

        #print("Pos:",observation.copter.position.x,observation.copter.position.y,"Dest:",observation.copter.destination.x,observation.copter.destination.y)

        """
        if self.reset_state:  # go to middle of board
            self.x = observation.copter.position.x # 36
            self.y = observation.copter.position.y # 64
            self.reset_state = False
            print('Reset')
        """
        if fire_count != 0:   # if there's fire
            direction = get_direction(calc_matrix.transpose())
            #print("FIRE DIRECTION",fire_direction[direction])
            self.x = observation.copter.position.x + direction[0]
            self.y = observation.copter.position.y + direction[1]
            #self.x = max(0, min(self.x, 71))  # fix this
            #self.y = max(0, min(self.y, 127))  # fix this
            #print("Pos:",observation.copter.position.x,observation.copter.position.y,"dir:",direction[0],direction[1],'New:',self.x,self.y)
        else:   # if there's no fire
            newx,newy,new_search_state = get_next_move(self.mono_gradient_matrix,self.count,observation.copter.position.x,observation.copter.position.y)

            if abs(observation.copter.position.x - observation.copter.destination.x) < 1 and abs(observation.copter.position.y - observation.copter.destination.y) < 1:
                #print('NO FIRE NEW DIRECTION')
                #self.x,self.y = my_random()
                #self.x,self.y,self.search_state = get_next_move(self.mono_gradient_matrix,self.count,observation.copter.position.x,observation.copter.position.y)
                self.x,self.y,self.search_state = newx,newy,new_search_state
                #print(get_next_move(self.mono_gradient_matrix,self.count))
                #print('RRR')

            else:
                if self.search_state == 'random':
                    if new_search_state == 'target':
                        self.x,self.y,self.search_state = newx,newy,new_search_state
                    else:
                        self.x,self.y = self.x,self.y
                elif self.search_state == 'target':
                    #print('NO FIRE CONTINUE')
                    #self.x = self.x  #observation.copter.destination.x
                    #self.y = self.y  #observation.copter.destination.y
                    #print("CCCC")
                    self.x,self.y,self.search_state = newx,newy,new_search_state



        p = action.path.add()
        p.x = observation.copter.position.x
        p.y = observation.copter.position.y

        p = action.path.add()
        p.x = self.x
        p.y = self.y

        #print('PPPP',p.x,p.y)


        return action

    def reward(self, reward):
        pass

    def end(self):
        pass


if __name__ == '__main__':
    server = GrpcServer(FakeHuman, cog_settings)
    server.serve()
