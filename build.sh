#!/bin/bash
set -e

# Prepare rust build environment
docker build -f ./Dockerfile.rust_build -t flames_rust_builder .

cogment generate --python_dir=. --js_dir=client/js/src

# Build website

# Wasm library
# JS
docker run --rm -v $(pwd)/common_sim:/src flames_rust_builder cargo build --manifest-path=/src/Cargo.toml --target wasm32-unknown-unknown --release
docker run --rm -v $(pwd)/common_sim:/src flames_rust_builder wasm-gc /src/target/wasm32-unknown-unknown/release/psim.wasm -o /src/psim.gc.wasm
cp common_sim/psim.gc.wasm client/js/src/psim.wasm

# Python
docker run --rm -v $(pwd)/common_sim:/src flames_rust_builder cargo build --manifest-path=/src/Cargo.toml --release
cp common_sim/target/release/libpsim.so ./

docker-compose build

docker-compose run build-webui

# Static files
cp -uR client/js/static/* client/js/dist/