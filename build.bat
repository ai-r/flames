REM #!/bin/bash
REM set -e
REM  Prepare rust build environment
docker build -f ./Dockerfile.rust_build -t flames_rust_builder .
cogment generate --python_dir=. --js_dir=client/js/src
REM # Build website
REM # Wasm library
REM # JS
docker run --rm -v %cd%/common_sim:/src flames_rust_builder cargo build --manifest-path=/src/Cargo.toml --target wasm32-unknown-unknown --release
docker run --rm -v %cd%/common_sim:/src flames_rust_builder wasm-gc /src/target/wasm32-unknown-unknown/release/psim.wasm -o /src/psim.gc.wasm
REM # Python
docker run --rm -v %cd%/common_sim:/src flames_rust_builder cargo build --manifest-path=/src/Cargo.toml --release
copy %cd%\common_sim\psim.gc.wasm %cd%\client\js\src\psim.wasm
REM following doesn't work
REM copy %cd%\common_sim\target\release\libpsim.so ./
copy %cd%\common_sim\target\release\libpsim.so %cd%
docker-compose build
docker-compose run build-webui
REM # Static files
REM you may have to create client directory if this fails
xcopy %cd%\client\js\static\* %cd%\client\js\dist\* /s /y
