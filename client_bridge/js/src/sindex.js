
import sGame from './sgame';

window.addEventListener("DOMContentLoaded", (event) => {
  const canvas = document.querySelector('#game_canvas');
  const conn_state = document.querySelector('#conn_status');

  conn_state.innerHTML = "Connecting...";
  window.game = new sGame(canvas, conn_state);

  console.log("Game started");
});