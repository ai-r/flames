import {Connection} from 'cogment';
import cog_settings from './cog_settings';

const pako = require('pako');
const protos = require("./data_pb")




function displaylogo() {

  const w = 600;
  const h = 400;

  var canvas = document.querySelector('#render');
  canvas.width = w;
  canvas.height = h;


  var ctx = canvas.getContext("2d");
  ctx.fillStyle = "black";
  ctx.fillRect(0, 0, canvas.width, canvas.height);

  
  var image_data = new Uint8ClampedArray(960000).fill(255);
  var image = new ImageData(image_data, w, h);
  //console.log('yea',image.data);


  ctx.putImageData(image, 0, 0);

  ctx.fillRect(20, 20, 150, 100);

}



function showmesg(t, form) {

   let area = document.forms[form + 'form'].getElementsByTagName('textarea')[0];

   area.value += t + '\n';
   area.scrollTop = area.scrollHeight;
} // end of showmesg




async function launch() {

  var counter = 0;
  var conn_created = false;
  // self.mouse_image = pygame.image.load('mouse.jpg')
  // var run_btn = document.querySelector("#run");
  // var games = document.querySelector('#games');
  //var t_name = document.querySelector('#t_name');
  
  var conn = new Connection(cog_settings, "http://127.0.0.1:8089")
  //var aom_conn = new Connection(cog_settings,"http://" + window.location.hostname + ":8088")
  //var aom_conn = new Connection(cog_settings, "http://3.19.169.7:8088")


  var trial = undefined;
  var ready = false;
  var running = false;
  var obs = undefined;

  //displaylogo();

//  run_btn.onclick = async function() {


/*
    if (!conn_created) {
      //var cfg = new protos.Config();
      //cfg.setEnvName('CartPole-v1');
      trial = await conn.start_trial(cog_settings.actor_classes.human_plane) //, cfg);
      //ready = true;
      conn_created = true;

      //display(trial.observation);
      run_btn.disabled = true;
    }   
*/

  var i;
  for (i=0; i < 3; i++) {
    showmesg('creating trial','test')
    showmesg('Loop Number:' + i,'test')
    trial = await conn.start_trial(cog_settings.actor_classes.human_plane) //, cfg);
    showmesg("Client Trial now Established",'test');
    var count = 0;
    while (true) {
      count++;
      showmesg('Client loop: ' + count,'test');
      var action = new protos.Loop_HumanAction();
      action.setTest('origin-' + count);
      obs = await trial.do_action(action);
      showmesg('Client action done','test');
      showmesg('obs: done: ' + obs.getDone(),'test');
      if (obs.getDone()) {
        showmesg('OOOOObservation Done','test');
        break;
      } // end of if done
    } // end of while true


    trial.end();
    trial = undefined;
    showmesg("Trial over, try again",'test');
    //run_btn.disabled = false;

//  };  // end of run_btn


  } // end of for loop

} // end of launch



// need to set following for order of things to happen properly
window.addEventListener("DOMContentLoaded", function() {
        console.log("Dom loaded");
        launch();
    }, false);


// window.setTimeout(launch, 1000);