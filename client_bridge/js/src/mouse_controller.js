import { LazyBrush, Point } from 'lazy-brush'

const TOUCH_LAZY_RADIUS = 5;
const MOUSE_LAZY_RADIUS = 30;
const SEGMENT_LEN = 10;
const MAX_PATH_LEN = 100;

function dist_sq(p1, p2) {
  const dx = p1.x - p2.x;
  const dy = p1.y - p2.y; 

  return dx * dx + dy * dy;
} 

class MouseController {
  constructor(command_callback) {
    this.lazy = null;

    this.path = [];
    this.path_end = null;
    this.object = null;
    this.command_callback = command_callback;
  }

  command_start(p, is_mouse, obj) {
    this.object = obj;
    if(window.navigator && window.navigator.vibrate) {
      window.navigator.vibrate(60);
    }
    p = obj.pos;
    this.lazy = new LazyBrush({
      radius: is_mouse ? MOUSE_LAZY_RADIUS : TOUCH_LAZY_RADIUS,
      enabled: true,
      initialPoint: p
    });
    this.path.push(p);
    this.path.push(p);

    this.path_end = p;
    console.log(this.path_end);
  }

  command_update(p) {
    if(!this.lazy) {
      return;
    }
    
    if(this.path.length >= MAX_PATH_LEN) {
      return ;
    }
    this.path.pop();

    // Break down the path into equal length segments
    const vec = {
      x: p.x - this.path_end.x,
      y: p.y - this.path_end.y
    };

    var dist = Math.sqrt(vec.x*vec.x + vec.y*vec.y);
    const dir = {
      x: vec.x / dist,
      y: vec.y / dist
    };


    while(dist > SEGMENT_LEN) {
      const next = {
        x: this.path_end.x + dir.x * SEGMENT_LEN,
        y: this.path_end.y + dir.y * SEGMENT_LEN
      };

      dist -= SEGMENT_LEN;

      this.lazy.update(next);
      this.path.push(this.lazy.getBrushCoordinates());
      this.path_end = next;
    }

    this.path.push(p);
  }

  command_end() {
    this.command_callback(this.object, this.path);
    this.lazy = null;
    this.path = [];
  }

  
}

export default MouseController;