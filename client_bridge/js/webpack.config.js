const path = require('path');

module.exports = {
  entry: './src/sindex.js',  // was './src/client_bridge.js',
  output: {
    filename: 'main_client_bridge.js',
    path: path.resolve(__dirname, 'dist')
  },
  optimization: {
		// We no not want to minimize our code.
		minimize: false
	},
};