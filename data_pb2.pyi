# @generated by generate_proto_mypy_stubs.py.  Do not edit!
import sys
from google.protobuf.descriptor import (
    Descriptor as google___protobuf___descriptor___Descriptor,
    EnumDescriptor as google___protobuf___descriptor___EnumDescriptor,
)

from google.protobuf.internal.containers import (
    RepeatedCompositeFieldContainer as google___protobuf___internal___containers___RepeatedCompositeFieldContainer,
    RepeatedScalarFieldContainer as google___protobuf___internal___containers___RepeatedScalarFieldContainer,
)

from google.protobuf.message import (
    Message as google___protobuf___message___Message,
)

from typing import (
    Iterable as typing___Iterable,
    List as typing___List,
    Optional as typing___Optional,
    Text as typing___Text,
    Tuple as typing___Tuple,
    cast as typing___cast,
)

from typing_extensions import (
    Literal as typing_extensions___Literal,
)


builtin___bool = bool
builtin___bytes = bytes
builtin___float = float
builtin___int = int
builtin___str = str


class CellState(builtin___int):
    DESCRIPTOR: google___protobuf___descriptor___EnumDescriptor = ...
    @classmethod
    def Name(cls, number: builtin___int) -> builtin___str: ...
    @classmethod
    def Value(cls, name: builtin___str) -> 'CellState': ...
    @classmethod
    def keys(cls) -> typing___List[builtin___str]: ...
    @classmethod
    def values(cls) -> typing___List['CellState']: ...
    @classmethod
    def items(cls) -> typing___List[typing___Tuple[builtin___str, 'CellState']]: ...
    UNKNOWN = typing___cast('CellState', 0)
    BLANK = typing___cast('CellState', 1)
    ON_FIRE = typing___cast('CellState', 2)
    TREE = typing___cast('CellState', 3)
    CHARRED = typing___cast('CellState', 4)
    DOUSED = typing___cast('CellState', 5)
    UNKNOWN_OLD = typing___cast('CellState', 32)
    BLANK_OLD = typing___cast('CellState', 33)
    ON_FIRE_OLD = typing___cast('CellState', 34)
    TREE_OLD = typing___cast('CellState', 35)
    CHARRED_OLD = typing___cast('CellState', 36)
    DOUSED_OLD = typing___cast('CellState', 37)
UNKNOWN = typing___cast('CellState', 0)
BLANK = typing___cast('CellState', 1)
ON_FIRE = typing___cast('CellState', 2)
TREE = typing___cast('CellState', 3)
CHARRED = typing___cast('CellState', 4)
DOUSED = typing___cast('CellState', 5)
UNKNOWN_OLD = typing___cast('CellState', 32)
BLANK_OLD = typing___cast('CellState', 33)
ON_FIRE_OLD = typing___cast('CellState', 34)
TREE_OLD = typing___cast('CellState', 35)
CHARRED_OLD = typing___cast('CellState', 36)
DOUSED_OLD = typing___cast('CellState', 37)

class Config(google___protobuf___message___Message):
    DESCRIPTOR: google___protobuf___descriptor___Descriptor = ...
    dumb_count = ... # type: builtin___int
    smart_count = ... # type: builtin___int
    smartest_count = ... # type: builtin___int
    pilot = ... # type: builtin___bool
    multiclient_count = ... # type: builtin___int

    def __init__(self,
        *,
        dumb_count : typing___Optional[builtin___int] = None,
        smart_count : typing___Optional[builtin___int] = None,
        smartest_count : typing___Optional[builtin___int] = None,
        pilot : typing___Optional[builtin___bool] = None,
        multiclient_count : typing___Optional[builtin___int] = None,
        ) -> None: ...
    @classmethod
    def FromString(cls, s: builtin___bytes) -> Config: ...
    def MergeFrom(self, other_msg: google___protobuf___message___Message) -> None: ...
    def CopyFrom(self, other_msg: google___protobuf___message___Message) -> None: ...
    if sys.version_info >= (3,):
        def ClearField(self, field_name: typing_extensions___Literal[u"dumb_count",u"multiclient_count",u"pilot",u"smart_count",u"smartest_count"]) -> None: ...
    else:
        def ClearField(self, field_name: typing_extensions___Literal[u"dumb_count",b"dumb_count",u"multiclient_count",b"multiclient_count",u"pilot",b"pilot",u"smart_count",b"smart_count",u"smartest_count",b"smartest_count"]) -> None: ...

class InitialForestState(google___protobuf___message___Message):
    DESCRIPTOR: google___protobuf___descriptor___Descriptor = ...
    initial_fires = ... # type: builtin___int
    drone_names = ... # type: google___protobuf___internal___containers___RepeatedScalarFieldContainer[typing___Text]
    drone_types = ... # type: google___protobuf___internal___containers___RepeatedScalarFieldContainer[builtin___int]

    def __init__(self,
        *,
        initial_fires : typing___Optional[builtin___int] = None,
        drone_names : typing___Optional[typing___Iterable[typing___Text]] = None,
        drone_types : typing___Optional[typing___Iterable[builtin___int]] = None,
        ) -> None: ...
    @classmethod
    def FromString(cls, s: builtin___bytes) -> InitialForestState: ...
    def MergeFrom(self, other_msg: google___protobuf___message___Message) -> None: ...
    def CopyFrom(self, other_msg: google___protobuf___message___Message) -> None: ...
    if sys.version_info >= (3,):
        def ClearField(self, field_name: typing_extensions___Literal[u"drone_names",u"drone_types",u"initial_fires"]) -> None: ...
    else:
        def ClearField(self, field_name: typing_extensions___Literal[u"drone_names",b"drone_names",u"drone_types",b"drone_types",u"initial_fires",b"initial_fires"]) -> None: ...

class Vec2(google___protobuf___message___Message):
    DESCRIPTOR: google___protobuf___descriptor___Descriptor = ...
    x = ... # type: builtin___float
    y = ... # type: builtin___float

    def __init__(self,
        *,
        x : typing___Optional[builtin___float] = None,
        y : typing___Optional[builtin___float] = None,
        ) -> None: ...
    @classmethod
    def FromString(cls, s: builtin___bytes) -> Vec2: ...
    def MergeFrom(self, other_msg: google___protobuf___message___Message) -> None: ...
    def CopyFrom(self, other_msg: google___protobuf___message___Message) -> None: ...
    if sys.version_info >= (3,):
        def ClearField(self, field_name: typing_extensions___Literal[u"x",u"y"]) -> None: ...
    else:
        def ClearField(self, field_name: typing_extensions___Literal[u"x",b"x",u"y",b"y"]) -> None: ...

class ObjectInfo(google___protobuf___message___Message):
    DESCRIPTOR: google___protobuf___descriptor___Descriptor = ...
    target_orientation = ... # type: builtin___float
    message = ... # type: typing___Text

    @property
    def position(self) -> Vec2: ...

    @property
    def destination(self) -> Vec2: ...

    def __init__(self,
        *,
        position : typing___Optional[Vec2] = None,
        destination : typing___Optional[Vec2] = None,
        target_orientation : typing___Optional[builtin___float] = None,
        message : typing___Optional[typing___Text] = None,
        ) -> None: ...
    @classmethod
    def FromString(cls, s: builtin___bytes) -> ObjectInfo: ...
    def MergeFrom(self, other_msg: google___protobuf___message___Message) -> None: ...
    def CopyFrom(self, other_msg: google___protobuf___message___Message) -> None: ...
    if sys.version_info >= (3,):
        def HasField(self, field_name: typing_extensions___Literal[u"destination",u"position"]) -> builtin___bool: ...
        def ClearField(self, field_name: typing_extensions___Literal[u"destination",u"message",u"position",u"target_orientation"]) -> None: ...
    else:
        def HasField(self, field_name: typing_extensions___Literal[u"destination",b"destination",u"position",b"position"]) -> builtin___bool: ...
        def ClearField(self, field_name: typing_extensions___Literal[u"destination",b"destination",u"message",b"message",u"position",b"position",u"target_orientation",b"target_orientation"]) -> None: ...

class Observation(google___protobuf___message___Message):
    DESCRIPTOR: google___protobuf___descriptor___Descriptor = ...
    grid_width = ... # type: builtin___int
    grid_height = ... # type: builtin___int
    drone_names = ... # type: google___protobuf___internal___containers___RepeatedScalarFieldContainer[typing___Text]
    drone_types = ... # type: google___protobuf___internal___containers___RepeatedScalarFieldContainer[builtin___int]
    cells = ... # type: google___protobuf___internal___containers___RepeatedScalarFieldContainer[CellState]
    cell_age = ... # type: google___protobuf___internal___containers___RepeatedScalarFieldContainer[builtin___int]
    done = ... # type: builtin___bool
    agent_type = ... # type: typing___Text
    remaining_forests = ... # type: builtin___float

    @property
    def copter(self) -> ObjectInfo: ...

    @property
    def drones(self) -> google___protobuf___internal___containers___RepeatedCompositeFieldContainer[ObjectInfo]: ...

    def __init__(self,
        *,
        grid_width : typing___Optional[builtin___int] = None,
        grid_height : typing___Optional[builtin___int] = None,
        copter : typing___Optional[ObjectInfo] = None,
        drones : typing___Optional[typing___Iterable[ObjectInfo]] = None,
        drone_names : typing___Optional[typing___Iterable[typing___Text]] = None,
        drone_types : typing___Optional[typing___Iterable[builtin___int]] = None,
        cells : typing___Optional[typing___Iterable[CellState]] = None,
        cell_age : typing___Optional[typing___Iterable[builtin___int]] = None,
        done : typing___Optional[builtin___bool] = None,
        agent_type : typing___Optional[typing___Text] = None,
        remaining_forests : typing___Optional[builtin___float] = None,
        ) -> None: ...
    @classmethod
    def FromString(cls, s: builtin___bytes) -> Observation: ...
    def MergeFrom(self, other_msg: google___protobuf___message___Message) -> None: ...
    def CopyFrom(self, other_msg: google___protobuf___message___Message) -> None: ...
    if sys.version_info >= (3,):
        def HasField(self, field_name: typing_extensions___Literal[u"copter"]) -> builtin___bool: ...
        def ClearField(self, field_name: typing_extensions___Literal[u"agent_type",u"cell_age",u"cells",u"copter",u"done",u"drone_names",u"drone_types",u"drones",u"grid_height",u"grid_width",u"remaining_forests"]) -> None: ...
    else:
        def HasField(self, field_name: typing_extensions___Literal[u"copter",b"copter"]) -> builtin___bool: ...
        def ClearField(self, field_name: typing_extensions___Literal[u"agent_type",b"agent_type",u"cell_age",b"cell_age",u"cells",b"cells",u"copter",b"copter",u"done",b"done",u"drone_names",b"drone_names",u"drone_types",b"drone_types",u"drones",b"drones",u"grid_height",b"grid_height",u"grid_width",b"grid_width",u"remaining_forests",b"remaining_forests"]) -> None: ...

class ObservationDelta(google___protobuf___message___Message):
    DESCRIPTOR: google___protobuf___descriptor___Descriptor = ...
    updated_cells_list = ... # type: google___protobuf___internal___containers___RepeatedScalarFieldContainer[builtin___int]
    new_cell_states = ... # type: google___protobuf___internal___containers___RepeatedScalarFieldContainer[CellState]
    done = ... # type: builtin___bool
    remaining_forests = ... # type: builtin___float

    @property
    def copter(self) -> ObjectInfo: ...

    @property
    def drones(self) -> google___protobuf___internal___containers___RepeatedCompositeFieldContainer[ObjectInfo]: ...

    def __init__(self,
        *,
        updated_cells_list : typing___Optional[typing___Iterable[builtin___int]] = None,
        new_cell_states : typing___Optional[typing___Iterable[CellState]] = None,
        copter : typing___Optional[ObjectInfo] = None,
        drones : typing___Optional[typing___Iterable[ObjectInfo]] = None,
        done : typing___Optional[builtin___bool] = None,
        remaining_forests : typing___Optional[builtin___float] = None,
        ) -> None: ...
    @classmethod
    def FromString(cls, s: builtin___bytes) -> ObservationDelta: ...
    def MergeFrom(self, other_msg: google___protobuf___message___Message) -> None: ...
    def CopyFrom(self, other_msg: google___protobuf___message___Message) -> None: ...
    if sys.version_info >= (3,):
        def HasField(self, field_name: typing_extensions___Literal[u"copter"]) -> builtin___bool: ...
        def ClearField(self, field_name: typing_extensions___Literal[u"copter",u"done",u"drones",u"new_cell_states",u"remaining_forests",u"updated_cells_list"]) -> None: ...
    else:
        def HasField(self, field_name: typing_extensions___Literal[u"copter",b"copter"]) -> builtin___bool: ...
        def ClearField(self, field_name: typing_extensions___Literal[u"copter",b"copter",u"done",b"done",u"drones",b"drones",u"new_cell_states",b"new_cell_states",u"remaining_forests",b"remaining_forests",u"updated_cells_list",b"updated_cells_list"]) -> None: ...

class Ai_DroneAction(google___protobuf___message___Message):
    DESCRIPTOR: google___protobuf___descriptor___Descriptor = ...
    message = ... # type: typing___Text

    @property
    def path(self) -> google___protobuf___internal___containers___RepeatedCompositeFieldContainer[Vec2]: ...

    def __init__(self,
        *,
        path : typing___Optional[typing___Iterable[Vec2]] = None,
        message : typing___Optional[typing___Text] = None,
        ) -> None: ...
    @classmethod
    def FromString(cls, s: builtin___bytes) -> Ai_DroneAction: ...
    def MergeFrom(self, other_msg: google___protobuf___message___Message) -> None: ...
    def CopyFrom(self, other_msg: google___protobuf___message___Message) -> None: ...
    if sys.version_info >= (3,):
        def ClearField(self, field_name: typing_extensions___Literal[u"message",u"path"]) -> None: ...
    else:
        def ClearField(self, field_name: typing_extensions___Literal[u"message",b"message",u"path",b"path"]) -> None: ...

class Human_PlaneAction(google___protobuf___message___Message):
    DESCRIPTOR: google___protobuf___descriptor___Descriptor = ...
    ai_override_target = ... # type: builtin___int

    @property
    def path(self) -> google___protobuf___internal___containers___RepeatedCompositeFieldContainer[Vec2]: ...

    @property
    def ai_override(self) -> Ai_DroneAction: ...

    def __init__(self,
        *,
        path : typing___Optional[typing___Iterable[Vec2]] = None,
        ai_override : typing___Optional[Ai_DroneAction] = None,
        ai_override_target : typing___Optional[builtin___int] = None,
        ) -> None: ...
    @classmethod
    def FromString(cls, s: builtin___bytes) -> Human_PlaneAction: ...
    def MergeFrom(self, other_msg: google___protobuf___message___Message) -> None: ...
    def CopyFrom(self, other_msg: google___protobuf___message___Message) -> None: ...
    if sys.version_info >= (3,):
        def HasField(self, field_name: typing_extensions___Literal[u"ai_override"]) -> builtin___bool: ...
        def ClearField(self, field_name: typing_extensions___Literal[u"ai_override",u"ai_override_target",u"path"]) -> None: ...
    else:
        def HasField(self, field_name: typing_extensions___Literal[u"ai_override",b"ai_override"]) -> builtin___bool: ...
        def ClearField(self, field_name: typing_extensions___Literal[u"ai_override",b"ai_override",u"ai_override_target",b"ai_override_target",u"path",b"path"]) -> None: ...

class Loop_HumanAction(google___protobuf___message___Message):
    DESCRIPTOR: google___protobuf___descriptor___Descriptor = ...
    ai_override_target = ... # type: builtin___int

    @property
    def path(self) -> google___protobuf___internal___containers___RepeatedCompositeFieldContainer[Vec2]: ...

    @property
    def ai_override(self) -> Ai_DroneAction: ...

    def __init__(self,
        *,
        path : typing___Optional[typing___Iterable[Vec2]] = None,
        ai_override : typing___Optional[Ai_DroneAction] = None,
        ai_override_target : typing___Optional[builtin___int] = None,
        ) -> None: ...
    @classmethod
    def FromString(cls, s: builtin___bytes) -> Loop_HumanAction: ...
    def MergeFrom(self, other_msg: google___protobuf___message___Message) -> None: ...
    def CopyFrom(self, other_msg: google___protobuf___message___Message) -> None: ...
    if sys.version_info >= (3,):
        def HasField(self, field_name: typing_extensions___Literal[u"ai_override"]) -> builtin___bool: ...
        def ClearField(self, field_name: typing_extensions___Literal[u"ai_override",u"ai_override_target",u"path"]) -> None: ...
    else:
        def HasField(self, field_name: typing_extensions___Literal[u"ai_override",b"ai_override"]) -> builtin___bool: ...
        def ClearField(self, field_name: typing_extensions___Literal[u"ai_override",b"ai_override",u"ai_override_target",b"ai_override_target",u"path",b"path"]) -> None: ...
