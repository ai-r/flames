REM ***************************************************************************
REM following deals with client_bridge javascript stuff
REM 
REM ***************************************************************************

REM need to copy following libraries/dependencies to client_bridge/js/src
copy %cd%\common_sim\psim.gc.wasm %cd%\client\js\src\psim.wasm
copy %cd%\client\js\src\cog_settings.js %cd%\client_bridge\js\src\cog_settings.js
copy %cd%\client\js\src\data_pb.js %cd%\client_bridge\js\src\data_pb.js
copy %cd%\client\js\src\delta.js %cd%\client_bridge\js\src\delta.js

docker-compose run build-webui-client

REM copy required webpack output files to the client/js/dist director
copy %cd%\client_bridge\js\dist\main_client_bridge.js %cd%\client\js\dist\main_client_bridge.js
copy %cd%\client_bridge\js\static\client_bridge.html %cd%\client\js\dist\client_bridge.html
copy %cd%\client_bridge\js\static\sindex.html %cd%\client\js\dist\sindex.html
copy %cd%\client_bridge\js\static\cb_game.html %cd%\client\js\dist\cb_game.html
copy %cd%\client_bridge\js\dist\1.main_client_bridge.js %cd%\client\js\dist\1.main_client_bridge.js
copy %cd%\client_bridge\js\dist\*.wasm %cd%\client\js\dist\*.wasm

