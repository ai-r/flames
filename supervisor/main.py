import cog_settings

import traceback
from cogment import TrialHooks, GrpcServer
from types import SimpleNamespace as ns

DUMB_URL = 'grpc://dumb:9000'
SMARTER_URL = 'grpc://smarter:9000'
SMARTEST_URL = 'grpc://smartest:9000'
FAKE_HUMAN_URL = 'grpc://fake-human:9000'
MULTICLIENT_URL = 'grpc://client-bridge:9000'



class PreHook_impl(TrialHooks):
    VERSIONS = {"flames": "1.0.0"}

    def pre_trial(self, trial_id, user_id, trial_params):
        try:
            print("before:" , str(trial_params.trial_config))

            # remove the default actors
            actors = [ns(
                actor_class='human_plane',
                endpoint="human",
                config=None
              )] + trial_params.trial_config.dumb_count * [
                ns(
                  actor_class='ai_drone',
                  endpoint=DUMB_URL,
                  config=None
                )
              ] + trial_params.trial_config.smart_count * [
                ns(
                  actor_class='ai_drone',
                  endpoint=SMARTER_URL,
                  config=None
                )
              ] + trial_params.trial_config.smartest_count * [
                ns(
                  actor_class='ai_drone',
                  endpoint=SMARTEST_URL,
                  config=None
                )
              ] + trial_params.trial_config.multiclient_count * [
                ns(
                  actor_class='loop_human',
                  endpoint=MULTICLIENT_URL,
                  config=None
                )
              ]

            drone_types = (trial_params.trial_config.dumb_count * [0]
                           + trial_params.trial_config.smart_count * [1]
                           + trial_params.trial_config.smartest_count * [2])

            drone_names = [f'{x}' for x, _ in enumerate(drone_types)]

            trial_params.environment.config.drone_names.extend(drone_names)
            trial_params.environment.config.drone_types.extend(drone_types)

            if trial_params.trial_config.pilot:
                actors.append(ns(
                  actor_class='human_plane',
                  endpoint=FAKE_HUMAN_URL,
                  config=None
                ))

            trial_params.actors = actors

            return trial_params
        except Exception:
            traceback.print_exc()
            raise


if __name__ == "__main__":
    server = GrpcServer(PreHook_impl, cog_settings)
    server.serve()
