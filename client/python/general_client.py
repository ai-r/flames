import cog_settings

#from data_pb2 import Human_PlaneAction
from data_pb2 import Loop_HumanAction
from cogment.client import Connection

import time
import os



def dump(obj):
  for attr in dir(obj):
    print("obj.%s = %r" % (attr, getattr(obj, attr)))

# Create a connection to the Orchestrator serving this project
#conn = Connection(cog_settings, "127.0.0.1:9000")
conn = Connection(cog_settings, "client-bridge:50051")

print('Client starting trial:')


for i in range(3):
	print('creating trial')
	print("Loop Number",i)
	# Initiate a trial
	trial = conn.start_trial(cog_settings.actor_classes.human_plane)  # whatever
	print('Client Trial now established')
	print('obsss',trial.id)  # observation is in trial

	#dump(trial.id)


	count = 0
	while True:
		count += 1
		print('Client loop:',count)
		#observation = trial.do_action(Human_PlaneAction())
		# action = Human_PlaneAction()
		# p = action.path.add()
		# p.x = 36
		# p.y = 64
		# action = Loop_HumanAction()
		# action.test = 'whatever'
		# observation = trial.do_action(action)


		# observation = trial.do_action(Loop_HumanAction(test='origin'+str(count)))

		action = Loop_HumanAction()
		p = action.path.add()
		p.x = 16
		p.y = 16
		observation = trial.do_action(action)




		print('Client action done')
		print("obs: done:",observation.done)
		if observation.done:
			print('OOOOOObservation Done!!')
			break


	trial.end()