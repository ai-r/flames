# client.py
import cog_settings

from data_pb2 import Human_PlaneAction, Config
from cogment.client import Connection
from clients.python.viewer import Viewer


# Create a connection to the Orchestrator serving this project
#conn = Connection(cog_settings, "127.0.0.1:9000")
conn = Connection(cog_settings, "orchestrator:9000")
viewer = Viewer()


# Initiate a trial
trial = conn.start_trial(cog_settings.actor_classes.human_plane, Config(agent_type = 'contourplus'))  # whatever

while True:
    observation = trial.do_action(Human_PlaneAction())
   	print("obs:",observation.done)
	if observation.done:
    	break
    else:
		viewer.show_(self.color_gradient_matrix,'image')


trial.end()