
import Game from './game';

window.addEventListener("DOMContentLoaded", (event) => {
  const canvas = document.querySelector('#game_canvas');
  const conn_state = document.querySelector('#conn_status');

  conn_state.innerHTML = "Connecting...";
  
  //window.tester = new Tester()
  window.game = new Game(canvas, conn_state);

  console.log("Game started");
});
