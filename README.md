# AI-r flames demo.

## Requirements

The project has been tested to work with cogment 0.2 and protoc 3.10

* [cogment](https://gitlab.com/cogment/cogment/-/releases/0.2)
* [protoc](https://github.com/protocolbuffers/protobuf/releases/tag/v3.10.1)

Reminder: 

* The cogment executable should be renamed to `cogment` or `cogment.exe` (on windows)
* Both `cogment` and `protoc` should be available on your PATH

## Building

The `build.sh` / `build.bat` script contains a shorthand script to build the project. It unconditionally builds all parts of the project at once, so it might be a bit overkill, but it's still fast enough to not be a bother at the moment.

## Running

`run.sh` / `run.bat` launches the project for basic human-in-the-loop experimentation.

You can access the game at `http://localhost` or `http://your-ip-here` from your favorite browser. 