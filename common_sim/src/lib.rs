#[repr(C)]
#[no_mangle]
#[derive(Debug, Clone, Copy)]
pub struct Vec2 {
    x: f32,
    y: f32,
}

#[repr(C)]
#[no_mangle]
#[derive(Debug, Clone, Copy)]
pub struct MoverInfo {
    pos: Vec2,
    dest: Vec2,
    vel: Vec2
}


#[no_mangle]
pub extern fn update_mover(mover: &mut MoverInfo, dt: f32, speed: f32) {
      let dest = mover.dest.clone();
      let mut pos = mover.pos.clone();
      let mut vel = mover.vel.clone();

      let delta = Vec2 {
        x: dest.x - pos.x,
        y: dest.y - pos.y,
      };

      let distance = (delta.x * delta.x + delta.y * delta.y).sqrt();
      let coverage = dt * (speed);
  
      if distance <= coverage {
        pos.x = dest.x;
        pos.y = dest.y;
        
        vel.x = 0.0;
        vel.y = 0.0;
      } else {
        let dx = delta.x / distance * coverage;
        let dy = delta.y / distance * coverage;
        vel.x *= 0.99;
        vel.y *= 0.99;

        vel.x += dx * speed * dt;
        vel.y += dy * speed * dt;

        pos.x += vel.x;
        pos.y += vel.y;
      } 

  mover.pos.x = pos.x;
  mover.pos.y = pos.y;
  mover.vel.x = vel.x;
  mover.vel.y = vel.y;

}


#[no_mangle]
pub unsafe extern fn js_new_mover() -> u32 {
  let ret = MoverInfo {
    pos: Vec2 {x: 1.0, y:2.0 },
    dest: Vec2 {x: 3.0, y:4.0 },
    vel: Vec2 {x: 5.0, y:6.0 }
  };

  Box::into_raw(Box::new(ret)) as u32
}

#[no_mangle]
pub unsafe extern fn js_delete_mover(mover: u32) {
  let mover = mover as *mut MoverInfo;
  drop(Box::from_raw(mover));
}
