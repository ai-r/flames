FROM python:3.7

RUN pip install cffi ptvsd pygame numpy scipy opensimplex prometheus_client opencv-python cogment==0.1.9 keras==2.2.4. tensorflow==1.14.0.

COPY . /app
WORKDIR /app
